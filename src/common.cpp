#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <common.h>

// depreacted: use hsv
RgbColor get_rgb_for_rainbow_percentage(int p) {
    RgbColor color;
    float part = 255 / 12;
    if (p < 3 * part) {
        color.red = 255 - map(p, 0, 3 * part, 0, 255);
    }
    if (p > 9 * part) {
        color.red = map(p, 9 * part, 255, 0, 255);
    }

    if (p > 2 * part and p < 8 * part) {
        color.green = 255 - abs(map(p, 2 * part, 8 * part, -255, 255));
    }

    if (p > 6 * part) {
        color.blue = 255 - abs(map(p, 6 * part, 255, -255, 255));
    }

    return color;
}