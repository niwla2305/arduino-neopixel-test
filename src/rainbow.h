#include <Adafruit_NeoPixel.h>

class RainbowEffect {
   public:
    RainbowEffect(Adafruit_NeoPixel &pixels) : _pixels(pixels) {}

    void tick() {
        Serial.println("rainbow tick");
        for (long firstPixelHue = 0; firstPixelHue < 65536; firstPixelHue += 256) {
            for (int i = 0; i < _pixels.numPixels(); i++) {  // For each pixel in strip...
                int pixelHue = firstPixelHue + (i * 65536L / _pixels.numPixels());
                _pixels.setPixelColor(i, _pixels.gamma32(_pixels.ColorHSV(pixelHue)));
            }
            _pixels.show();  // Update strip with new contents
            delay(10);       // Pause for a moment
        }
    }

   private:
    Adafruit_NeoPixel &_pixels;
};