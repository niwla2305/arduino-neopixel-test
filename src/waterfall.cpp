#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void waterfall_effect(Adafruit_NeoPixel pixels, int delay_time) {
    pixels.clear();
    randomSeed(analogRead(A0));

    uint16_t history[pixels.numPixels()];

    while (true) {
        long color = pixels.ColorHSV(2000, random(248, 255), 255);

        int before_c = color;
        for (int c = 1; c < pixels.numPixels(); c++) {
            uint16_t history_c = history[c];
            history[c] = before_c;
            before_c = history_c;
        }
        history[0] = color;

        for (int i = 0; i < pixels.numPixels(); i++) {
            pixels.setPixelColor(pixels.numPixels() - i, history[i]);
        }
        pixels.show();
        delay(delay_time);
    }
}