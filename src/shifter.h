#include <Adafruit_NeoPixel.h>

class ShifterEffect {
   public:
    ShifterEffect(Adafruit_NeoPixel pixels) {
        _pixels = pixels;
        _shift = 0;
    }

    void tick() {
        for (int i = 0; i < _pixels.numPixels(); i++) {
            _pixels.setPixelColor(i + _shift, _pixels.Color(255, 0, 0));
            _pixels.setPixelColor(i + _shift + 1, _pixels.Color(0, 255, 0));
            _pixels.setPixelColor(i + _shift + 2, _pixels.Color(0, 0, 255));
            i += 8;
            _pixels.show();
        }
        if (_shift == 3) {
            _shift = 0;
        } else {
            _shift++;
        }

        delay(100);
    }

   private:
    Adafruit_NeoPixel _pixels;
    int _shift;
};