#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>
// #include <etl/deque.h>

void development_effect(Adafruit_NeoPixel pixels, int num_pixels) {
    pixels.clear();
    randomSeed(analogRead(A0));

    uint32_t available_colors[] = {
        pixels.Color(0, 255, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(255, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(255, 255, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),
        pixels.Color(0, 0, 0),

    };

    uint32_t current_color = 0;
    uint32_t history[pixels.numPixels()];

    while (true) {
        uint32_t color = available_colors[current_color];

        uint32_t before_c = color;
        for (int c = 1; c < pixels.numPixels(); c++) {
            uint32_t history_c = history[c];
            history[c] = before_c;
            before_c = history_c;
        }
        history[0] = color;

        for (int i = 0; i < pixels.numPixels(); i++) {
            pixels.setPixelColor(pixels.numPixels() - i, history[i]);
        }
        pixels.show();
        delay(60);
        if (current_color >= 15) {
            current_color = 0;
        } else {
            current_color++;
        }
    }
}