#include <Adafruit_NeoPixel.h>

class StroboEffect {
   public:
    StroboEffect(Adafruit_NeoPixel pixels, int on_time, int off_time) {
        _pixels = pixels;
        _on_time = on_time;
        _off_time = off_time;
    }

    void tick() {
        Serial.println("strobo tick");
        _pixels.clear();
        for (int i = 0; i < _pixels.numPixels(); i++) {
            _pixels.setPixelColor(i, _pixels.Color(255, 255, 255));
        }
        _pixels.show();
        delay(_on_time);
        for (int i = 0; i < _pixels.numPixels(); i++) {
            _pixels.setPixelColor(i, _pixels.Color(0, 0, 0));
        }
        _pixels.show();
        delay(_off_time);
    }

   private:
    Adafruit_NeoPixel _pixels;
    int _on_time;
    int _off_time;
};