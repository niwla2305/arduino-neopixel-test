#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void multitrain_effect(Adafruit_NeoPixel pixels, float delaytime) {
    pixels.clear();

    int blank_pixels = 8;

    uint32_t color_set[] = {0xff0000, 0x0000ff, 0x00ff00, 0x00ffff, 0xff00ff, 0xffff00};
    int num_colors = sizeof(color_set) / sizeof(*color_set);

    int chain_len = num_colors * (blank_pixels + 1);
    uint32_t available_colors[chain_len];

    int count = 0;
    for (size_t color = 0; color < num_colors; color++) {
        Serial.println("color");
        available_colors[count] = color_set[color];
        count++;
        for (size_t pixel = 0; pixel < blank_pixels; pixel++) {
            available_colors[count] = 0;
            count++;
        }
    }

    uint32_t current_color = 0;
    uint32_t history[pixels.numPixels()];

    while (true) {
        uint32_t color = available_colors[current_color];

        // moves all values in history
        uint32_t before_c = color;
        for (size_t c = 1; c < pixels.numPixels(); c++) {
            uint32_t history_c = history[c];
            history[c] = before_c;
            before_c = history_c;
        }
        history[0] = color;

        for (size_t i = 0; i < pixels.numPixels(); i++) {
            pixels.setPixelColor(pixels.numPixels() - i, history[i]);
        }
        pixels.show();
        delay(delaytime);
        if (current_color > chain_len - 2) {
            current_color = 0;
        } else {
            current_color++;
        }
    }
}
