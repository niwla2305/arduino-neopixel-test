from PIL import Image

def range_map(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def percent_to_rgb(p):
    r=g=b = 0
    part = 255/12

    if p < 3*part:
        r = 255 - range_map(p, 0, 3*part, 0, 255)

    if p > 9*part:
        r = range_map(p, 9*part, 255, 0, 255)

    if p > 2*part and p < 8*part:
        g = 255 - abs(range_map(p, 2*part, 8*part, -255, 255))

    if p > 6*part:
        b = 255 - abs(range_map(p, 6*part, 255, -255, 255))


    return (r, g, b)

image = Image.new(mode="RGB", size=(256,40))
pixels = image.load()
for i in range(255):
    print(i, percent_to_rgb(i))
    for j in range(40):
        pixels[i,j] = percent_to_rgb(i)

image.save("temp.png")
